import pandas as pd
lottery_data = pd.read_csv('Lottery_data.csv')


lottery_data.columns = ['Date', 'lottery no 1', 'lottery 2 last','lottery 3 font','lottery 3 last']
# print(lottery_data)


#---------------------------------------------- lottery no 1 ------------------------------------
lottery_no1 = lottery_data.copy()
del lottery_no1['lottery 2 last']
del lottery_no1['lottery 3 font']
del lottery_no1['lottery 3 last']
# print(lottery_no1)
# lottery_no1.info()


lottery_no1.reset_index(inplace=True)
dothis_merged_lottery_no1 = lambda x: pd.Series([i for i in reversed(x.split('/'))])
dates = lottery_no1['Date'].apply(dothis_merged_lottery_no1)
merged_lottery_no1_to = pd.merge(lottery_no1, dates,  how='inner', left_index=True, right_index=True)
del merged_lottery_no1_to['index']
merged_lottery_no1_to.columns = ['Date','lottery no 1','Year','Month','Day']
del merged_lottery_no1_to['Year']
del merged_lottery_no1_to['Month']
del merged_lottery_no1_to['Day']
merged_lottery_no1_to['lottery no 1'] = merged_lottery_no1_to['lottery no 1'].astype(int)

# merged_lottery_2_last_to.info()
# print(merged_lottery_no1_to.head(20))

#------------------------------------------- lottery 2 last ------------------------------------
lottery_2_last = lottery_data.copy()
del lottery_2_last['lottery no 1']
del lottery_2_last['lottery 3 font']
del lottery_2_last['lottery 3 last']
# print(lottery_2_last)

lottery_2_last.reset_index(inplace=True)

dothis_merged_lottery_2_last = lambda x: pd.Series([i for i in reversed(x.split('/'))])
dates = lottery_2_last['Date'].apply(dothis_merged_lottery_2_last)
merged_lottery_2_last_to = pd.merge(lottery_2_last, dates,  how='inner', left_index=True, right_index=True)
del merged_lottery_2_last_to['index']
merged_lottery_2_last_to.columns = ['Date','lottery 2 last','Year','Month','Day']
del merged_lottery_2_last_to['Year']
del merged_lottery_2_last_to['Month']
del merged_lottery_2_last_to['Day']
merged_lottery_2_last_to['lottery 2 last'] = merged_lottery_2_last_to['lottery 2 last'].astype(int)

# merged_lottery_2_last_to.info()
# print(merged_lottery_2_last_to.head(20))
#------------------------------------------- lottery 3 font ------------------------------------
lottery_3_font = lottery_data.copy()
del lottery_3_font['lottery 2 last']
del lottery_3_font['lottery no 1']
del lottery_3_font['lottery 3 last']
# print(lottery_3_font.head(20))

lottery_3_font_to = pd.DataFrame(lottery_3_font['lottery 3 font'].str.split(" ").apply(pd.Series, 0).stack())
lottery_3_font_to.index = lottery_3_font_to.index.droplevel(-1)
# print(lottery_3_font_to.head(20))

merged_lottery_3_font = pd.merge(lottery_data, lottery_3_font_to,  how='inner', left_index=True, right_index=True)
del merged_lottery_3_font['lottery no 1']
del merged_lottery_3_font['lottery 2 last']
del merged_lottery_3_font['lottery 3 font']
del merged_lottery_3_font['lottery 3 last']
merged_lottery_3_font.columns = ['Date','lottery 3 font']
merged_lottery_3_font.reset_index(inplace=True)

dothis_merged_lottery_3_font = lambda x: pd.Series([i for i in reversed(x.split('/'))])
dates = merged_lottery_3_font['Date'].apply(dothis_merged_lottery_3_font)
merged_lottery_3_font_to = pd.merge(merged_lottery_3_font, dates,  how='inner', left_index=True, right_index=True)
del merged_lottery_3_font_to['index']
merged_lottery_3_font_to.columns = ['Date','lottery 3 font','Year','Month','Day']
del merged_lottery_3_font_to['Year']
del merged_lottery_3_font_to['Month']
del merged_lottery_3_font_to['Day']
merged_lottery_3_font_to['lottery 3 font'] = merged_lottery_3_font_to['lottery 3 font'].astype(int)

# merged_lottery_3_font_to.info()
# print(merged_lottery_3_font_to.head(20))

#------------------------------------------- lottery 3 last ------------------------------------
lottery_3_last = lottery_data.copy()
del lottery_3_last['lottery 2 last']
del lottery_3_last['lottery 3 font']
del lottery_3_last['lottery no 1']
# print(lottery_3_last.head(20))

lottery_3_last_to = pd.DataFrame(lottery_3_last['lottery 3 last'].str.split(" ").apply(pd.Series, 0).stack())
lottery_3_last_to.index = lottery_3_last_to.index.droplevel(-1)
# print(lottery_3_last_to.head(20))

merged_lottery_3_last = pd.merge(lottery_data, lottery_3_last_to,  how='inner', left_index=True, right_index=True)
del merged_lottery_3_last['lottery no 1']
del merged_lottery_3_last['lottery 2 last']
del merged_lottery_3_last['lottery 3 font']
del merged_lottery_3_last['lottery 3 last']
merged_lottery_3_last.columns = ['Date','lottery 3 last']
merged_lottery_3_last.reset_index(inplace=True)

dothis_merged_lottery_3_last = lambda x: pd.Series([i for i in reversed(x.split('/'))])
dates = merged_lottery_3_last['Date'].apply(dothis_merged_lottery_3_last)
merged_lottery_3_last_to = pd.merge(merged_lottery_3_last, dates,  how='inner', left_index=True, right_index=True)
del merged_lottery_3_last_to['index']
merged_lottery_3_last_to.columns = ['Date','lottery 3 last','Year','Month','Day']
del merged_lottery_3_last_to['Year']
del merged_lottery_3_last_to['Month']
del merged_lottery_3_last_to['Day']
merged_lottery_3_last_to['lottery 3 last'] = merged_lottery_3_last_to['lottery 3 last'].astype(int)

# merged2.info()
# print(merged_lottery_3_last_to.head(20))

#------------------------------------------- predict lottery 2 last ------------------------------------
index_lottery_2_last = pd.Index(merged_lottery_2_last_to['lottery 2 last'])
count_lottory_2_last = index_lottery_2_last.value_counts()
data_count_lottory_2_last = count_lottory_2_last.reset_index()
data_count_lottory_2_last.columns = ['lottery 2 last', 'count']
# print(data_count_lottory_2_last)
# print(data_count_lottory_2_last.head(20)) 

#------------------------------------------- predict lottery 3 font ------------------------------------
index_lottery_3_font = pd.Index(merged_lottery_3_font_to['lottery 3 font'])
count_lottory_3_font = index_lottery_3_font.value_counts() 
data_count_lottory_3_font = count_lottory_3_font.reset_index()
data_count_lottory_3_font.columns = ['lottery 3 font', 'count']
# print(data_count_lottory_3_font)
# print(data_count_lottory_3_font.head(20)) 

#------------------------------------------- predict lottery 3 last ------------------------------------
index_lottery_3_last = pd.Index(merged_lottery_3_last_to['lottery 3 last'])
count_lottory_3_last = index_lottery_3_last.value_counts() 
data_count_lottory_3_last = count_lottory_3_last.reset_index()
data_count_lottory_3_last.columns = ['lottery 3 last', 'count']
# print(data_count_lottory_3_last)
# print(data_count_lottory_3_last.head(20)) 