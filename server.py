# pip install
# pip install python-simplexml
# pip install Flask
# pip install Flask-RESTful
# pip install Flask-Cors
# pip install pandas

from simplexml import dumps
from flask import make_response, Flask, json, request, jsonify
from flask_restful import Api, Resource
from flask_cors import CORS, cross_origin
from Modellottery import merged_lottery_3_last_to
from Modellottery import merged_lottery_3_font_to
from Modellottery import merged_lottery_2_last_to
from Modellottery import merged_lottery_no1_to
from Modellottery import data_count_lottory_2_last
from Modellottery import data_count_lottory_3_font
from Modellottery import data_count_lottory_3_last


# --------------------------------------------------------
def output_xml(data, code, headers=None):
    resp = make_response(dumps({'response': data}), code)
    resp.headers.extend(headers or {})
    return resp

def output_json(data, code, headers=None):
    resp = make_response(json.dumps(data), code)
    resp.headers.extend(headers or {})
    return resp

app = Flask(__name__)
CORS(app)
api = Api(app, default_mediatype='application/xml')
api.representations['application/json'] = output_json
# --------------------------------------------------------

class Helloworld(Resource):
    def get(self):
        return {'Helloworld':'Hello Word'}



class lottery_no1(Resource):
    def get(self):
        lottery_no1 = merged_lottery_no1_to['lottery no 1']
        return  {"lotteryNo1" : lottery_no1.to_json(orient='records')}


class lottery_2_last(Resource):
    def get(self):
        lottery_2_last = merged_lottery_2_last_to['lottery 2 last']
        return  {"lottery2last" : lottery_2_last.to_json(orient='records')}

class lottery_3_font(Resource):
    def get(self):
        lottery_3_font = merged_lottery_3_font_to['lottery 3 font']
        return  {"lottery3font" :lottery_3_font.to_json(orient='records')}

class lottery_3_last(Resource):
    def get(self):
        lottery_3_last = merged_lottery_3_last_to['lottery 3 last']
        return  {"lottery3last" : lottery_3_last.to_json(orient='records')}





class lottery_data_no1(Resource):
    def get(self):
        lottery_data_no1 = merged_lottery_no1_to
        return  {"lotterydataNo1" : lottery_data_no1.to_json(orient='records')}

class lottery_data_2_last(Resource):
    def get(self):
        lottery_data_2_last = merged_lottery_2_last_to
        return  {"lotterydata2last" : lottery_data_2_last.to_json(orient='records')}

class lottery_data_3_font(Resource):
    def get(self):
        lottery_data_3_font = merged_lottery_3_font_to
        return  {"lotterydata3font" : lottery_data_3_font.to_json(orient='records')}

class lottery_data_3_last(Resource):
    def get(self):
        lottery_data_3_last = merged_lottery_3_last_to
        return  {"lotterydata3last" : lottery_data_3_last.to_json(orient='records')}





class lottery_data_2_last_count(Resource):
    def get(self):
        count1_lottery_data_2_last = data_count_lottory_2_last['lottery 2 last']
        count2_lottery_data_2_last = data_count_lottory_2_last['count']
        return  {"lotterydata2lastcount1" : count1_lottery_data_2_last.to_json(orient='records'),"lotterydata2lastcount2" : count2_lottery_data_2_last.to_json(orient='records')}



class lottery_data_3_font_count(Resource):
    def get(self):
        count1_lottery_data_3_font = data_count_lottory_3_font['lottery 3 font']
        count2_lottery_data_3_font = data_count_lottory_3_font['count']
        return  {"lotterydata3fontcount1" : count1_lottery_data_3_font.to_json(orient='records'),"lotterydata3fontcount2" : count2_lottery_data_3_font.to_json(orient='records')}


class lottery_data_3_last_count(Resource):
    def get(self):
        count1_lottery_data_3_last = data_count_lottory_3_last['lottery 3 last']
        count2_lottery_data_3_last = data_count_lottory_3_last['count']
        return  {"lotterydata3lastcount1" : count1_lottery_data_3_last.to_json(orient='records'),"lotterydata3lastcount2" : count2_lottery_data_3_last.to_json(orient='records')}

api.add_resource(Helloworld,'/')

api.add_resource(lottery_no1,'/lotteryno1')
api.add_resource(lottery_2_last,'/lottery2last')
api.add_resource(lottery_3_font,'/lottery3font')
api.add_resource(lottery_3_last,'/lottery3last')

api.add_resource(lottery_data_no1,'/lotterydatano1')
api.add_resource(lottery_data_2_last,'/lotterydata2last')
api.add_resource(lottery_data_3_font,'/lotterydata3font')
api.add_resource(lottery_data_3_last,'/lotterydata3last')

api.add_resource(lottery_data_2_last_count,'/lotterydata2lastcount')
api.add_resource(lottery_data_3_font_count,'/lotterydata3fontcount')
api.add_resource(lottery_data_3_last_count,'/lotterydata3lastcount')



# -----------------------------------------------------------
if __name__ == '__main__':
    # app.run(host='127.0.0.1', debug=False, port=4080)
    app.run(host='178.128.111.212', debug=False, port=77)
# -----------------------------------------------------------
